/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 00:30:28 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/01 20:18:11 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include <mlx.h>
# include <math.h>
# include <stdlib.h>
# include "../libft/libft.h"
# define X_START 200
# define Y_START 200
# define MLX_WIN_WIDTH 1200
# define MLX_WIN_HEIGHT 1200
# define MLX_WIN_NAME "Display Map with MiniLIBX"
# define HIGHEST_COLOR 0xFF0000
# define LOWEST_COLOR 0x0000FF
# define G global()

typedef struct			s_bres
{
	int					dx;
	int					sx;
	int					dy;
	int					sy;
	int					err;
	int					e2;
}						t_bres;

typedef struct			s_point
{
	int					x;
	int					y;
	int					z;
	int					color;
}						t_point;

typedef struct			s_mlx
{
	int					width;
	int					height;
	void				*mlx_ptr;
	void				*win_ptr;
	void				*img_ptr;
	char				*img_data;
	int					bpp;
	int					size_line;
	int					endian;
}						t_mlx;

typedef struct			s_projection
{
	char				isometric;
	char				cavalier;
	char				cabine;
}						t_projection;

typedef struct			s_global
{
	t_point				**map;
	char				is_colored_map;
	short				highest_point;
	short				lowest_point;
	int					line_len;
	int					count_lines;
	struct s_mlx		mlx;
	struct s_projection projections;
	float				scale;
	float				scale_offset;
	short				padding_x;
	short				padding_y;
	short				shift_x;
	short				shift_y;
	int					error_state;
}						t_global;

void					error_processor(char *str, int code);
int						open_file(char *filename);
void					usage(void);
t_global				*global(void);
void					parse_map(int fd);
void					update_stepp_pad();
void					new_ptr_line_to_map(int line_len);
int						splits_count(char **points);
void					init_mlx(int width, int height);
int						display_map(int key_code, void *data);
void					key_processor(int key_code, t_global *gl);
t_point					isometric(t_point x);
t_point					cavalier(t_point x);
t_point					cabine(t_point x);
t_point					scale_shift_color(t_global *gl, t_point point);
void					mlx_img_clear(void);
void					destroy_mlx(void);
void					mlx_img_put_pixel(int x, int y, int color);
void					free_map(void);
int						get_color(t_point current, t_point start, t_point end);
int						quit(void *param);

#endif
