# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/30 14:57:31 by ishyian           #+#    #+#              #
#    Updated: 2019/07/03 14:57:00 by ishyian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC =				gcc
NAME_FDF =			fdf
CFLAGS =			-Wall -Wextra -Werror
SRCS_FDF =			./src/fdf.c \
					./src/usage.c \
					./src/error_processor.c \
					./src/new_ptr_line_to_map.c \
					./src/open_file.c \
					./src/global.c \
					./src/parse_map.c \
					./src/splits_count.c \
					./src/init_destroy_mlx.c \
					./src/mlx_img.c \
					./src/free_map.c \
					./src/display_map.c \
					./src/key_processor.c \
					./src/get_color.c \
					./src/update_stepp_pad.c \
					./src/quit.c \
					./src/isometric.c \
					./src/cabine.c \
					./src/cavalier.c \
					./src/scale_shift_color.c

OBJ_FDF =			$(SRCS_FDF:.c=.o)
LIBS = 				-lft -lncurses -lmlx -framework OpenGL -framework Appkit
LIB_DIR =			./libft/
LIB_FT =			./libft/libft.a
INCLUDES =			./inc/fdf.h

all: 				$(NAME_FDF)

$(NAME_FDF):		$(LIB_FT) $(OBJ_FDF)
					$(CC) $(CFLAGS) -o $(NAME_FDF) $(OBJ_FDF) $(LIBS) -L $(LIB_DIR)

$(OBJ_FDF): 		%.o: %.c $(INCLUDES)
					$(CC) $(CFLAGS) -I $(INCLUDES) -o $@ -c $<

$(LIB_FT):			libft
					make -C $(LIB_DIR)

libft:
					make -C $(LIB_DIR)

clean:
					rm -f $(OBJ_FDF)
					make clean -C $(LIB_DIR)

fclean: 			clean
					rm -f $(NAME_FDF)
					make fclean -C $(LIB_DIR)

re: 				fclean all

.PHONY: 			all libft clean fclean
