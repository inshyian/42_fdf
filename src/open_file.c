/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   open_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 15:55:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 03:31:43 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include "../inc/fdf.h"

int				open_file(char *filename)
{
	int			fd;

	fd = open(filename, O_RDONLY);
	if (fd < 0 || (read(fd, 0, 0) < 0))
		error_processor(ft_strdup(filename), 1);
	return (fd);
}
