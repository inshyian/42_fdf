/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 12:05:36 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/24 13:44:16 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

int				main(int ac, char **av)
{
	int			fd;

	if (ac < 2 || ac > 2)
		usage();
	fd = open_file(av[1]);
	ft_bzero(global(), sizeof(t_global));
	G->scale = 1.0;
	G->scale_offset = 1.0;
	G->shift_x = 0;
	G->shift_y = 0;
	parse_map(fd);
	update_stepp_pad();
	init_mlx(MLX_WIN_WIDTH, MLX_WIN_HEIGHT);
	display_map(0, NULL);
	destroy_mlx();
	free_map();
	return (0);
}
