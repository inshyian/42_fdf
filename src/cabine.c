/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cabine.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 13:27:33 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 14:39:01 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

t_point			cabine(t_point point)
{
	t_point		cab;
	float		m;
	float		n;

	m = 0.5 * cos(45 * M_PI / 180);
	n = -0.5 * sin(45 * M_PI / 180);
	cab.x = point.x + point.z * m;
	cab.y = point.y + point.z * n;
	cab.color = point.color;
	return (cab);
}
