/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_img.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/16 22:59:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 11:43:42 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void			mlx_img_clear(void)
{
	ft_memset(G->mlx.img_data, 0, MLX_WIN_HEIGHT * MLX_WIN_WIDTH * 4);
}

/*
** 	(G->mlx.img_data + to_zero_byte)[2] = (color >> 16) & 0xFF;
** 	(G->mlx.img_data + to_zero_byte)[1] = (color >> 8) & 0xFF;
** 	(G->mlx.img_data + to_zero_byte)[0] = color & 0xFF;
*/

void			mlx_img_put_pixel(int x, int y, int color)
{
	int		to_zero_byte;

	if (x < 0 || y < 0 || x > MLX_WIN_WIDTH || y > MLX_WIN_HEIGHT)
		return ;
	to_zero_byte = x * 4 + G->mlx.size_line * y;
	*(int *)(G->mlx.img_data + to_zero_byte) = color;
}
