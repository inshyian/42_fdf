/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 03:02:46 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/24 13:12:39 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static t_point	get_delta(t_point start, t_point end)
{
	t_point		delta;

	delta.x = ABS(DIFF(start.x, end.x));
	delta.y = ABS(DIFF(start.y, end.y));
	delta.z = ABS(DIFF(start.z, end.z));
	return (delta);
}

static float	percent(int start, int end, int current)
{
	float		placement;
	float		distance;

	placement = current - start;
	distance = end - start;
	return ((distance == 0) ? 1.0 : (placement / distance));
}

static int		get_light(int start, int end, double percentage)
{
	return ((int)((1 - percentage) * start + percentage * end));
}

int				get_color(t_point current, t_point start, t_point end)
{
	int			red;
	int			green;
	int			blue;
	float		percentage;
	t_point		delta;

	delta = get_delta(start, end);
	if (current.color == end.color)
		return (current.color);
	if (delta.x > delta.y)
		percentage = percent(start.x, end.x, current.x);
	else
		percentage = percent(start.y, end.y, current.y);
	red = get_light((start.color >> 16) & 0xFF, (end.color >> 16) & 0xFF,
																	percentage);
	green = get_light((start.color >> 8) & 0xFF, (end.color >> 8) & 0xFF,
																	percentage);
	blue = get_light(start.color & 0xFF, end.color & 0xFF, percentage);
	return ((red << 16) | (green << 8) | blue);
}
