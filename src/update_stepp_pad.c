/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_stepp_pad.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 11:35:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/24 15:20:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static void		update_stepp(t_global *gl, short stepp)
{
	int			i;
	int			ii;

	gl->padding_x = (MLX_WIN_WIDTH - (stepp * gl->line_len)) / 2;
	gl->padding_y = (MLX_WIN_HEIGHT - (stepp * gl->count_lines)) / 2;
	i = 0;
	while (i < (gl->count_lines))
	{
		ii = 0;
		while (ii < (gl->line_len))
		{
			gl->map[i][ii].x = gl->map[i][ii].x * stepp + gl->padding_x;
			gl->map[i][ii].y = gl->map[i][ii].y * stepp + gl->padding_y;
			gl->map[i][ii].z = gl->map[i][ii].z * stepp;
			ii++;
		}
		i++;
	}
}

static short	get_stepp(t_global *gl)
{
	short		stepp;

	if (gl->line_len >= gl->count_lines)
		stepp = ((MLX_WIN_WIDTH) - ((X_START) * 2)) / (gl->line_len);
	else
		stepp = ((MLX_WIN_HEIGHT) - ((Y_START) * 2)) / (gl->count_lines);
	return (stepp);
}

void			update_stepp_pad(void)
{
	t_global	*gl;
	short		stepp;

	gl = G;
	stepp = get_stepp(gl);
	update_stepp(gl, stepp);
	gl->lowest_point = gl->lowest_point * stepp;
	gl->highest_point = gl->highest_point * stepp;
}
