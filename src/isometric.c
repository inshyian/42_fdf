/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isometric.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 13:08:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 15:17:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

t_point			isometric(t_point point)
{
	t_point		iso;

	iso.x = (point.x - point.y) * cos(0.523599) + (MLX_WIN_WIDTH / 2);
	iso.y = -point.z + (point.x + point.y) * sin(0.523599);
	iso.color = point.color;
	return (iso);
}
