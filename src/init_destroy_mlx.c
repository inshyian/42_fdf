/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_destroy_mlx.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 17:09:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 13:16:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void			destroy_mlx(void)
{
	mlx_destroy_window(G->mlx.mlx_ptr, G->mlx.win_ptr);
	free(G->mlx.mlx_ptr);
}

void			init_mlx(int width, int height)
{
	G->mlx.width = width;
	G->mlx.height = height;
	G->mlx.mlx_ptr = mlx_init();
	G->mlx.win_ptr = mlx_new_window(G->mlx.mlx_ptr, width, height,
									MLX_WIN_NAME);
	G->mlx.img_ptr = mlx_new_image(G->mlx.mlx_ptr, width, height);
	G->mlx.img_data = mlx_get_data_addr(G->mlx.img_ptr, &G->mlx.bpp,
										&G->mlx.size_line, &G->mlx.endian);
	mlx_hook(G->mlx.win_ptr, 2, 0, display_map, 0);
	mlx_hook(G->mlx.win_ptr, 17, 0, quit, 0);
}
