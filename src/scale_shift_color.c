/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scale_shift_color.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 14:20:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/24 17:20:22 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static int		get_light(int start, int end, float percentage)
{
	return ((int)((1 - percentage) * start + percentage * end));
}

static int		get_col(float percentage, int start, int end)
{
	int			red;
	int			green;
	int			blue;

	red = get_light((start >> 16) & 0xFF, (end >> 16) & 0xFF, percentage);
	green = get_light((start >> 8) & 0xFF, (end >> 8) & 0xFF, percentage);
	blue = get_light(start & 0xFF, end & 0xFF, percentage);
	return ((red << 16) | (green << 8) | blue);
}

static float	percent(int start, int end, int current)
{
	float		placement;
	float		distance;

	placement = current - start;
	distance = end - start;
	return ((distance == 0) ? 1.0 : (placement / distance));
}

static int		color_by_altitude(t_global *gl, int z)
{
	short		lowest;
	short		highest;
	short		current;
	int			color;
	float		percentage;

	if (z == gl->lowest_point)
		return (LOWEST_COLOR);
	if (z == gl->highest_point)
		return (HIGHEST_COLOR);
	if (gl->lowest_point < 0)
	{
		lowest = 0;
		current = ABS(z) + ABS(gl->lowest_point);
		highest = ABS(gl->highest_point) + ABS(gl->lowest_point);
	}
	else
	{
		lowest = gl->lowest_point;
		current = z;
		highest = gl->highest_point;
	}
	percentage = percent(lowest, highest, current);
	color = get_col(percentage, LOWEST_COLOR, HIGHEST_COLOR);
	return (color);
}

t_point			scale_shift_color(t_global *gl, t_point point)
{
	t_point		new;

	new.x = (point.x - gl->padding_x) * gl->scale;
	new.y = (point.y - gl->padding_y) * gl->scale;
	new.x = gl->padding_x * gl->scale_offset + new.x + gl->shift_x;
	new.y = gl->padding_y * gl->scale_offset + new.y + gl->shift_y;
	new.z = point.z * gl->scale;
	if (gl->is_colored_map)
		new.color = point.color;
	else
		new.color = color_by_altitude(gl, point.z);
	return (new);
}
