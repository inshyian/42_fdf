/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 01:16:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 01:26:28 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void			free_map(void)
{
	t_point		**map;
	int			i;

	i = 0;
	map = G->map;
	while (i < G->count_lines)
	{
		free(map[i]);
		i++;
	}
	free(map);
}
