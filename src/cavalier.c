/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cavalier.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 13:27:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 14:38:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

t_point			cavalier(t_point point)
{
	t_point		cav;
	float		m;
	float		n;

	m = 1 * cos(45 * M_PI / 180);
	n = -1 * sin(45 * M_PI / 180);
	cav.x = point.x + point.z * m;
	cav.y = point.y + point.z * n;
	cav.color = point.color;
	return (cav);
}
