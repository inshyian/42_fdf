/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 13:13:32 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 01:25:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static void		process_usage(void)
{
	ft_printf("Usage:\t./fdf <map>.fdf\n");
}

void			usage(void)
{
	process_usage();
	exit(0);
}
