/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_ptr_line_to_map.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 15:04:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/23 01:20:10 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static void		addend_new_ptr_line_to_map(t_global *gl, int line_len)
{
	int			i;
	t_point		**new;

	i = 0;
	while (gl->map[i])
		i++;
	i++;
	new = (t_point **)ft_memalloc(sizeof(t_point *) * (i + 1));
	ft_memmove(new, gl->map, sizeof(t_point *) * i);
	free(gl->map);
	gl->map = new;
	gl->map[i - 1] = malloc(sizeof(t_point) * (line_len + 1));
	gl->map[i] = NULL;
}

void			new_ptr_line_to_map(int line_len)
{
	t_global	*gl;

	gl = G;
	if (!gl->map)
		gl->map = (t_point **)ft_memalloc(sizeof(t_point *) * 1);
	addend_new_ptr_line_to_map(gl, line_len);
}
