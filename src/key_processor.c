/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_processor.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 12:26:17 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/24 16:23:49 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static void		change_scale(int key_code, t_global *gl)
{
	if (key_code == 27 && gl->scale > 2.0)
	{
		gl->scale_offset = gl->scale_offset + 1.0;
		gl->scale = gl->scale - 1.0;
	}
	else if (key_code == 27 && gl->scale > 0.2)
	{
		gl->scale_offset = gl->scale_offset + 0.1;
		gl->scale = gl->scale - 0.1;
	}
	else if (key_code == 24 && gl->scale < 2.0)
	{
		gl->scale_offset = gl->scale_offset - 0.1;
		gl->scale = gl->scale + 0.1;
	}
	else if (key_code == 24 && gl->scale < 100.0)
	{
		gl->scale_offset = gl->scale_offset - 1.0;
		gl->scale = gl->scale + 1.0;
	}
}

static void		change_shift(int key_code, t_global *gl)
{
	if (key_code == 124)
		gl->shift_x = gl->shift_x - 50;
	else if (key_code == 123)
		gl->shift_x = gl->shift_x + 50;
	else if (key_code == 125)
		gl->shift_y = gl->shift_y - 50;
	else if (key_code == 126)
		gl->shift_y = gl->shift_y + 50;
}

static void		change_projection(int key_code, t_global *gl)
{
	ft_bzero(&gl->projections, sizeof(t_projection));
	if (key_code == 19)
		gl->projections.cabine = 1;
	else if (key_code == 20)
		gl->projections.cavalier = 1;
	else if (key_code == 21)
		gl->projections.isometric = 1;
}

void			key_processor(int kc, t_global *gl)
{
	if (kc == 18 || kc == 19 || kc == 20 || kc == 21)
		change_projection(kc, gl);
	else if (kc == 123 || kc == 124 || kc == 125 || kc == 126)
		change_shift(kc, gl);
	else if (kc == 27 || kc == 24)
		change_scale(kc, gl);
	else if (kc == 53)
		quit(NULL);
}
