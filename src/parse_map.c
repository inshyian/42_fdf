/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 14:30:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/03 15:03:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

static void			parse_point(t_global *gl, char **points, int x, int y)
{
	char		*coma;

	gl->map[y][x].x = x;
	gl->map[y][x].y = y;
	gl->map[y][x].z = ft_atoi(points[x]);
	if (gl->map[y][x].z > 100)
		gl->map[y][x].z = 100;
	else if (gl->map[y][x].z < -100)
		gl->map[y][x].z = -100;
	if ((coma = ft_strchr(points[x], ',')))
		gl->map[y][x].color = ft_getint(coma + 1);
	else
		gl->map[y][x].color = 0xFFFFFF;
	if (gl->map[y][x].color != 0xFFFFFF)
		gl->is_colored_map = 1;
	if (gl->map[y][x].z < gl->lowest_point)
		gl->lowest_point = gl->map[y][x].z;
	if (gl->map[y][x].z > gl->highest_point)
		gl->highest_point = gl->map[y][x].z;
}

static void			parse_points(t_global *gl, char **points, int y)
{
	int			splits_n;
	int			x;

	x = 0;
	splits_n = splits_count(points);
	if (!gl->line_len)
		gl->line_len = splits_n;
	new_ptr_line_to_map(splits_n);
	while (points[x])
	{
		if (gl->line_len == splits_n)
			parse_point(gl, points, x, y);
		else
			gl->error_state = 1;
		free(points[x]);
		x++;
	}
	free(points);
	if (gl->line_len != splits_n || gl->line_len == 0)
		gl->error_state = 1;
}

void				parse_map(int fd)
{
	char			*line;
	char			**points;
	t_global		*gl;
	int				y;

	y = 0;
	gl = G;
	while (get_next_line(fd, &line) == 1 && !gl->error_state)
	{
		points = ft_strsplit(line, ' ');
		free(line);
		parse_points(gl, points, y);
		y++;
	}
	gl->count_lines = y;
	if (!gl->map)
		gl->error_state = 1;
	if (gl->error_state || (gl->count_lines == 1 && gl->line_len == 1))
	{
		free_map();
		error_processor(NULL, 2);
	}
}
