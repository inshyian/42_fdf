/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_processor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 12:09:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 18:30:19 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void		error_processor(char *str, int code)
{
	ft_printf("%{bold,red}yError(%{white}y%d%{red}y): %{bold,white}y", code);
	if (code == 1)
		ft_fprintf(2, "InvalidMapFileName%{normal}y (%s)\n", str);
	if (code == 2)
		ft_fprintf(2, "InvalidMap%{normal}y\n", str);
	if (str)
		free(str);
	exit(0);
}
