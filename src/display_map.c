/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 01:18:14 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/24 15:12:16 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "../inc/fdf.h"

static void		paint_line(t_point curr, t_point a, t_point b)
{
	t_bres		brei;

	brei.dx = ABS(b.x - a.x);
	brei.sx = a.x < b.x ? 1 : -1;
	brei.dy = ABS(b.y - a.y);
	brei.sy = a.y < b.y ? 1 : -1;
	brei.err = (brei.dx > brei.dy ? brei.dx : -brei.dy) / 2;
	while (1)
	{
		mlx_img_put_pixel(curr.x, curr.y, get_color(curr, a, b));
		if (curr.x == b.x && curr.y == b.y)
			break ;
		brei.e2 = brei.err;
		if (brei.e2 > -brei.dx)
		{
			brei.err -= brei.dy;
			curr.x += brei.sx;
		}
		if (brei.e2 < brei.dy)
		{
			brei.err += brei.dx;
			curr.y += brei.sy;
		}
	}
}

static t_point	proj(t_global *gl, t_point point)
{
	point = scale_shift_color(gl, point);
	if (gl->projections.cabine)
		return (cabine(point));
	else if (gl->projections.cavalier)
		return (cavalier(point));
	else if (gl->projections.isometric)
		return (isometric(point));
	else
		return (point);
}

static void		paint_map(t_global *gl)
{
	int			i;
	int			ii;

	i = 0;
	while (i < (gl->count_lines))
	{
		ii = 0;
		while (ii < (gl->line_len))
		{
			if (ii < (gl->line_len - 1))
				paint_line(proj(gl, gl->map[i][ii]), proj(gl, gl->map[i][ii]),
							proj(gl, gl->map[i][ii + 1]));
			if (i < (gl->count_lines - 1))
				paint_line(proj(gl, gl->map[i][ii]), proj(gl, gl->map[i][ii]),
							proj(gl, gl->map[i + 1][ii]));
			ii++;
		}
		i++;
	}
	mlx_put_image_to_window(G->mlx.mlx_ptr, G->mlx.win_ptr,
							G->mlx.img_ptr, 0, 0);
	mlx_do_sync(G->mlx.mlx_ptr);
	mlx_img_clear();
}

int				display_map(int key_code, void *data)
{
	t_global	*gl;

	(void)data;
	gl = G;
	key_processor(key_code, gl);
	paint_map(gl);
	mlx_loop(gl->mlx.mlx_ptr);
	return (0);
}
